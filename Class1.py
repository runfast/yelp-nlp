import numpy as np
from collections import Counter
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import csv
import nltk
import gensim
from nltk import RegexpTokenizer
from gensim.models.ldamodel import LdaModel
from gensim.models import Word2Vec
from gensim import corpora

# Flags:
import_and_tokenize = 1 # set to 1 to import and tokenize from csv file
run_ldamodel = 1  # set to 1 to run LDA model or to 2 to load model from 'modelsave' below; anything else won't run fn
run_w2vmodel = 0  # set to 1 to run Word2Vec model


# File locations:
fileloc = 'C:\\Users\\DS1\\Desktop\\yelp.csv'
modelsave = 'model5.gensim'

def print_topics(model):
    print('Topics selected...')
    topics = model.print_topics(num_words=5)
    for topic in topics:
        print(topic)

if import_and_tokenize == 1:
    print('Working on importing CSV...')
    csvlist = []
    with open(fileloc, newline='') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            csvlist.append(row)
    #for i in range(5): print(csvlist[i])
    print('Working on tokenizer...')
    tokenizer = RegexpTokenizer(r'\w+')
    tokens = []
    en_stop = set(nltk.corpus.stopwords.words('english'))
    for i in range(len(csvlist)):
        tokens.append(tokenizer.tokenize(csvlist[i][4].lower()))
        tokens[i] = [word for word in tokens[i] if word not in en_stop]

# create or load LDA model that specifies n topics found in text
if run_ldamodel == 1:
    print('Working on LDA model...')
    numtopics = 10
    dictionary = corpora.Dictionary(tokens)
    corpus = [dictionary.doc2bow(text) for text in tokens]
    ldamodel = LdaModel(corpus, num_topics=numtopics, id2word=dictionary, passes=15)
    ldamodel.save(modelsave)
    print_topics(ldamodel)
elif run_ldamodel == 2:
    ldamodel = LdaModel.load(modelsave)
    print_topics(ldamodel)


if run_w2vmodel == 1:
    print('Working on Word2Vec...')
    wmodel = Word2Vec(tokens, iter=3, min_count=3)
    searchword = 'service'
    print('Most similar to ', searchword, ': ', wmodel.wv.most_similar(searchword, topn=5))

# import pyLDAvis.gensim
# lda_display = pyLDAvis.gensim.prepare(**lda)
# lda_display